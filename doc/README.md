# Vitrine Documentation

Inspired by ["The documentation system"](https://documentation.divio.com/).

1. [Reference](reference.md): information-oriented documentation, exhaustive and technical (should be updated everytime Vitrine evolves)
2. [How-to](howto.md): problem-oriented guides, for end users with specific needs (should be updated everytime we identify something's not clear for users)
